const express = require('express');
const app = express();
const bodyParser = require('body-parser')
const notFound = require('./middleware/not-found');
const errorHandlerMiddleware = require('./middleware/error-handler');
const ShelvesRouter = require('./routes/ShelvesRouter');
const BooksRouter = require('./routes/BooksRouter');
//require('dotenv').config();

// middleware

//app.use(express.static('./public'));
//app.use(express.json());

//routes
app.use(bodyParser.json())
app.use('/api/v1/shelves', ShelvesRouter);
app.use('/api/v1/books', BooksRouter);

app.use(notFound);
app.use(errorHandlerMiddleware);

host = '0.0.0.0'
port = 3000;

const start = async () => {
  try {
    app.listen(port, host, () =>
      console.log(`Server is listening on ${host}:${port}`)
    );
  } catch (error) {
    console.log(error);
  }
};

start();
