# GIT_SSH_COMMAND='ssh -i ~/.ssh/gitlab-ssh-askhat.practice1' git clone git@gitlab.com:askhat.practice1/AN-4-implementing-map.git
# GIT_SSH_COMMAND='ssh -i ~/.ssh/gitlab-ssh-askhat.practice1' git clone git@gitlab.com:askhat.practice1/AN-4-implementing-map.git

DOCKER_COMPOSE=docker-compose
APP=nodejs

.PHONY: all ps logs up stop restart down test shell pull node r

all: ps

ps: 
	$(DOCKER_COMPOSE) ps

logs:
	$(DOCKER_COMPOSE) logs -f --tail 100

up:
	$(DOCKER_COMPOSE) up -d --build
	$(DOCKER_COMPOSE) ps

stop:
	$(DOCKER_COMPOSE) stop

restart:
	$(DOCKER_COMPOSE) restart

down:
	$(DOCKER_COMPOSE) down

test:
	$(DOCKER_COMPOSE) run $(APP) sh -c "npm install && npm test"

shell: 
	$(DOCKER_COMPOSE) exec $(APP) bash
	
pull:
	git config core.sshCommand "ssh -i ~/.ssh/gitlab-ssh-askhat.practice1 -F /dev/null"
	git pull

push:
	git config core.sshCommand "ssh -i ~/.ssh/gitlab-ssh-askhat.practice1 -F /dev/null"
	git push

node:
	$(DOCKER_COMPOSE) exec $(APP) sh -c "node" 

bash:
	$(DOCKER_COMPOSE) exec $(APP) bash
