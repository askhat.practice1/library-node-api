const { library_db_file } = require("../common/env")
const { createCustomError } = require("../errors/custom-error")
const ShelvesModel = require("../models/ShelvesModel")
const scheme = require("../validators/general_validation")

/*
const id = req.params.id;
const userId = req.query.userId;
const data = req.body;
// */

const get_all_shelves = async (req, res, next) => {
    try {
        // validation block 
        const validation = scheme.validate({
            userId: req.query.userId,
        })
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }
        // action block
        const s = new ShelvesModel(library_db_file);
        const shelves = await s.get_shelves()
        console.log("shelves=", shelves)
        res.status(200).json(shelves)
    } catch (error) {
        console.log(error)
        next(error)
    }
}

const add_new_shelf = async (req, res, next) => {
    try {
        // validation block 
        const validation = scheme.validate({
            userId: req.query.userId,
            librarianId: req.query.userId,
            shelfId: req.body.shelfId,
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }
        // action block
        const { shelfId } = validation.value;
        const s = new ShelvesModel(library_db_file);
        const shelves = await s.add_shelf(shelfId)
        console.log("shelves=", shelves)
        res.status(201).json(shelves)
    } catch (error) {
        next(error)
    }
}

const get_all_books_on_the_shelf = async (req, res, next) => {
    try {
        // validation block 
        const validation = scheme.validate({
            userId: req.query.userId,
            shelfId: req.params.shelfId,
        })
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }
        // action block
        const { shelfId } = validation.value;
        const s = new ShelvesModel(library_db_file);
        const result = await s.get_shelf(shelfId)
        console.log("result=", result)
        res.status(200).json(result)
    } catch (error) {
        console.log(error)
        next(error)
    }
}

module.exports = {
    get_all_shelves,
    add_new_shelf,
    get_all_books_on_the_shelf
}     
