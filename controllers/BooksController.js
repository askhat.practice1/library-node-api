const { library_db_file } = require("../common/env")
const BooksModel = require('../models/BooksModel');
const { createCustomError } = require("../errors/custom-error")
const scheme = require("../validators/general_validation")

/*
const id = req.params.id;
const userId = req.query.userId;
const data = req.body;
// */

const getBook = async (req, res, next) => {
    try {
        // validation block
        const validation = scheme.validate({
            bookId: req.params.bookId,
            userId: req.query.userId,
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }
        // action block
        const { bookId } = validation.value;
        const b = new BooksModel(library_db_file);
        const book = await b.get_book(bookId)
        console.log("book=", book)
        res.status(200).json(book)
    }
    catch (error) {
        console.log(error)
        next(error)
    }
}

const searchBooks = async (req, res, next) => {
    try {
        // validation block
        const result = scheme.validate({
            userId: req.query.userId,
            name: req.query.name,
            ISBN: req.query.ISBN,
            author: req.query.author,
            genre: req.query.genre,
        })
        console.log(result)
        if (result.error) {
            throw createCustomError(result.error.message, 400);
        }
        // action block
        const { name, ISBN, author, genre } = result.value;
        const b = new BooksModel(library_db_file);
        let query = await b.get_all_books();
        console.log(typeof query);
        // filtering block
        if (name) {
            let filteredQuery = {};
            for (const i in query) {
                if (query[i].name.toLowerCase().includes(name.toLowerCase())) {
                    filteredQuery[i] = query[i];
                }
            }
            query = filteredQuery;
        }
        if (ISBN) {
            // if error check case of all ISBN in db 
            let filteredQuery = {};
            for (const i in query) {
                if (query[i].ISBN.toLowerCase().includes(ISBN.toLowerCase())) {
                    filteredQuery[i] = query[i];
                }
            }
            query = filteredQuery;
        }
        if (author) {
            let filteredQuery = {};
            for (const i in query) {
                if (query[i].author.toLowerCase().includes(author.toLowerCase())) {
                    filteredQuery[i] = query[i];
                }
            }
            query = filteredQuery;
        }
        if (genre) {
            let filteredQuery = {};
            for (const i in query) {
                if (query[i].genre.toLowerCase().includes(genre.toLowerCase())) {
                    filteredQuery[i] = query[i];
                }
            }
            query = filteredQuery;
        }
        console.log("query=", query);
        // response block
        if (Object.keys(query).length === 0) {
            throw createCustomError("No books found", 404);
        }
        res.status(200).json(query);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

const putBook = async (req, res, next) => {
    try {
        // validation block
        const validation = scheme.validate({
            userId: req.query.userId,
            librarianId: req.query.userId,
            book: req.body,
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }
        // action block
        const { book } = validation.value;
        const b = new BooksModel(library_db_file);
        const new_book = await b.put_book(book)
        console.log("new_book=", new_book)
        res.status(201).json(new_book);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

const moveBook = async (req, res, next) => {
    try {
        const validation = scheme.validate({
            userId: req.query.userId,
            librarianId: req.query.userId,
            bookId: req.params.bookId,
            shelfId: req.params.shelfId
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }

        const { bookId, shelfId } = validation.value;
        const b = new BooksModel(library_db_file);
        const result = await b.move_book(bookId, shelfId)

        res.status(200).json(result);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

const takeBook = async (req, res, next) => {
    try {
        const validation = scheme.validate({
            userId: req.query.userId,
            readerId: req.query.userId,
            bookId: req.params.bookId,
            period: req.query.period
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }

        const { userId, bookId, period } = validation.value;
        const b = new BooksModel(library_db_file);
        const result = await b.take_book(bookId, userId, period)

        res.status(200).json(result);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}
const returnBook = async (req, res, next) => {
    try {
        const validation = scheme.validate({
            userId: req.query.userId,
            readerId: req.query.userId,
            bookId: req.params.bookId,
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }

        const { userId, bookId } = validation.value;
        const b = new BooksModel(library_db_file);
        const result = await b.return_book(bookId, userId)

        res.status(200).json(result);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

const searchTakenBooks = async (req, res, next) => {
    try {
        // validation block
        const result = scheme.validate({
            userId: req.query.userId,
            librarianId: req.query.userId,
        })
        console.log(result)
        if (result.error) {
            throw createCustomError(result.error.message, 400);
        }
        // action block
        const b = new BooksModel(library_db_file);
        let query = await b.get_all_books();
        console.log(typeof query);
        // filtering block
        let filteredQuery = {};
        for (const i in query) {
            if (query[i].taken_by) {
                filteredQuery[i] = query[i];
            }
        }
        query = filteredQuery;
        // response block
        if (Object.keys(query).length === 0) {
            throw createCustomError("No books taken", 200);
        }
        res.status(200).json(query);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

const reserveBook = async (req, res, next) => {
    try {
        const validation = scheme.validate({
            userId: req.query.userId,
            readerId: req.query.userId,
            bookId: req.params.bookId,
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }

        const { userId, bookId } = validation.value;
        const b = new BooksModel(library_db_file);
        const result = await b.reserve_book(bookId, userId)

        res.status(200).json(result);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

const cancelBookReservation = async (req, res, next) => {
    try {
        const validation = scheme.validate({
            userId: req.query.userId,
            readerId: req.query.userId,
            bookId: req.params.bookId,
        })
        console.log(validation)
        if (validation.error) {
            throw createCustomError(validation.error.message, 400);
        }

        const { userId, bookId } = validation.value;
        const b = new BooksModel(library_db_file);
        const result = await b.unreserve_book(bookId, userId)

        res.status(200).json(result);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

const myReservedList = async (req, res, next) => {
    try {
        // validation block
        const result = scheme.validate({
            userId: req.query.userId,
            readerId: req.query.userId,
        })
        console.log(result)
        if (result.error) {
            throw createCustomError(result.error.message, 400);
        }
        // action block
        const { readerId } = result.value;
        const b = new BooksModel(library_db_file);
        let query = await b.get_all_books();
        console.log(typeof query);
        // filtering block
        let filteredQuery = {};
        for (const i in query) {
            if (query[i].reserved_by == readerId) {
                filteredQuery[i] = query[i];
            }
        }
        query = filteredQuery;
        // response block
        if (Object.keys(query).length === 0) {
            throw createCustomError("No books reserved", 200);
        }
        res.status(200).json(query);
    }
    catch (error) {
        console.log(error);
        next(error);
    }
}

module.exports = {
    searchBooks,
    putBook,
    getBook,
    moveBook,
    searchTakenBooks,
    takeBook,
    returnBook,
    reserveBook,
    cancelBookReservation,
    myReservedList,
}     
