# API Endpoint
- Tasks:
    - 1. A librarian and a reader should know which bookshelf the book is on.
    - 1.b. Every book has a unique ID in our system.
    - 4. A librarian should be able to get the information on a book using its ID.
    - 5.a. A librarian should know which reader took a book.
- Controller: `getBook`
- Method: `GET`
- Path: `/api/v1/books/:bookId?userId=1`
- Can access: `Librarian or Reader`

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `bookId` | `string` | The ID of a book you want to get details about. |

### Query Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The ID of the user. |

### Request Body

This endpoint does not require a request body.

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully received book details including shelf ID. |
| `401` | User is not authorized to access this resource. |
| `404` | There is no book with the provided ID. |
| `500` | Something went wrong on the server. |

### Response Body on Success

```json
{
  "id": "string",
  "isbn": "string",
  "name": "string",
  "author": "string",
  "genre": "string",
  "shelf": "string",
  "taken_by": "string",
  "taken_untill": "string",
  "reserved_by": "string"
}
```

# API Endpoint

- Tasks:
    - 1.a. The book could be found by the ISBN code or its name.
    - 1.c. We can have multiple books that match the criteria of our search, and they can be located on multiple bookshelves.
    - 1.d. It is possible to search for books by genre or author.
- Controller: `searchBooks`
- Method: `GET`
- Path: `/books?userId=1&ISBN=123&name=Lord&genre=Fantasy&author=Tolkien`
- Can access: `Librarian or Reader`

### Query Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The ID of the user. |
| `name` | `string` | [Optional] Search by name of the book. |
| `ISBN` | `string` | [Optional] Search by ISBN code. |
| `genre` | `string` | [Optional] Search by genre of the book. |
| `author` | `string` | [Optional] Search by author of the book. |

### Request Body

This endpoint does not require a request body.

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully received books details by search criteria. |
| `404` | There is no book with the provided query. |
| `500` | Something went wrong on the server. |

### Response Body on Success

```json
{
  "string":{
    "id": "string",
    "isbn": "string",
    "name": "string",
    "author": "string",
    "genre": "string",
    "shelf": "string",
    "taken_by": "string",
    "taken_untill": "string",
    "reserved_by": "string"
  }
}
```
# API Endpoint
- Tasks:
    - 2. A librarian should be able to put a new book on a shelf.
- Method: `POST`
- Path: `/books`
- Can access: `Librarian`

### Request Body

```json
{
  "isbn": "string",
  "name": "string",
  "author": "string",
  "genre": "string"
}
```

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `201` | Successfully put a new book to the shelf. |
| `404` | There is no ID or shelf ID found in the library. |
| `500` | Something went wrong on the server. |

# API Endpoint
- Tasks:
 - 3. A librarian should be able to move a book between shelves.
- Method: `PUT`
- Path: `/books/:bookId/shelf/:shelfId`
- Can access: `Librarian`

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `bookId` | `string` | The book ID. |
| `shelfId` | `string` | The shelf ID to move the book to. |

### Query Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userID` | `string` | The ID of the user. |

### Request Body

This endpoint does not require a request body.

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully moved the book to the shelf. |
| `401` | User is not authorized to access this resource. |
| `404` | There is no book ID or shelf ID found in the library. |
| `500` | Something went wrong on the server. |

### Response Body on Success

```json
{
  "id": "string",
  "isbn": "string",
  "name": "string",
  "author": "string",
  "genre": "string",
  "shelf": "string",
  "taken_by": "string",
  "taken_untill": "string",
  "reserved_by": "string"
}
```

# API Endpoint
- Tasks:
    - 5. A reader should be able to take a book to read it during some time period.
- Method: `PUT`
- Path: `/take_book/:bookId`
- Can access: `Reader`

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `bookId` | `string` | The book ID. |

### Query Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The user ID. |
| `period` | `integer` | The period of time in days. |

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully took the book. |
| `404` | There is no book ID or shelf ID found in the library. |
| `500` | Something went wrong on the server. |
# API Endpoint
- Method: `DELETE`
- Path: `/take_book/:bookId`
- Can access: `Reader`

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `bookId` | `string` | The book ID. |


### Request Body

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The user ID. |
| `period` | `integer` | The period of time in days. |

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully returned the book. |
| `404` | There is no book ID or shelf ID found in the library. |
| `500` | Something went wrong on the server. |

# API Endpoint
- Tasks:
    - 5.b. A librarian should know which books have been taken and when they will be returned.
- Method: `GET`
- Path: `/taken_books`
- Can access: `Librarian`

### Request Body

This endpoint does not require a request body.

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully received all taken books. |
| `400` | No taken books found. |
| `500` | Something went wrong on the server. |

### Response Body on Success

```json
[
  {
    "id": "string",
    "isbn": "string",
    "name": "string",
    "author": "string",
    "genre": "string",
    "shelf": "string",
    "taken_by": "string",
    "taken_untill": "string",
    "reserved_by": "string"
  }
]
```

# API Endpoint

- Tasks
    - 6. A reader should be able to reserve a book to read it next if it has been taken by someone else.
    - 6.a. If a book is already reserved for a specific period of time, we should reject this request.
- Method: `POST`
- Path: `/reserve_book/:bookId`
- Can access: `Reader`

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `bookId` | `string` | The book ID. |

### Request Body

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The user ID. |

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully reserved the book. |
| `404` | There is no book ID or shelf ID found in the library. |
| `500` | Something went wrong on the server. |

# API Endpoint
- Tasks:
    - 6.b. A reader should be able to reject a reservation.
- Method: `DELETE`
- Path: `/reserve_book/:bookId`
- Can access: `Reader`

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `bookId` | `string` | The book ID. |

### Request Body

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The user ID. |

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully rejected the reservation. |
| `404` | There is no book ID or shelf ID found in the library. |
| `500` | Something went wrong on the server. |


# API Endpoint
- Tasks:
    - 6.c. A reader should know which books they have reserved.
- Method: `GET`
- Path: `/reserved_books`
- Can access: `Reader`

### Request Body

This endpoint does not require a request body.

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully received all reserved books. |
| `400` | No reserved books found. |
| `500` | Something went wrong on the server. |

### Response Body on Success

```json
[
  {
    "id": "string",
    "isbn": "string",
    "name": "string",
    "author": "string",
    "genre": "string",
    "shelf": "string",
    "taken_by": "string",
    "taken_untill": "string",
    "reserved_by": "string"
  }
]
```

# API Endpoint
- Tasks:
    - 7. A librarian and a reader should be able to know what books are located on a shelf.
    - 7.a. Every shelf has a unique number, its ID.
- Method: `GET`
- Path: `/api/v1/shelves/:shelfId?userId=1`
- Can access: `Librarian or Reader`

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `shelfId` | `string` | The shelf ID. |

### Query Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The user ID. |

### Request Body

This endpoint does not require a request body.

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully received books in the shelf. |
| `400` | No users found. |
| `500` | Something went wrong on the server. |

### Response Body on Success

```json
[
  {
    "id": "string",
    "isbn": "string",
    "name": "string",
    "author": "string",
    "genre": "string",
    "shelf": "string",
    "taken_by": "string",
    "taken_untill": "string",
    "reserved_by": "string"
  }
]
```

# Supplementary API Endpoint
- Tasks:
    - 0. get list of all shelves in the library
- Method: `GET`
- Path: `/api/v1/shelves?userId=1`
- Can access: `Librarian or Reader`

### Query Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| `userId` | `string` | The user ID. |

### Request Body

This endpoint does not require a request body.

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `200` | Successfully received list of all shelves. |
| `404` | No shelves found. |
| `500` | Something went wrong on the server. |

### Response Body on Success

```json
[
  {
    "id": "string",
    "name": "string",
    "location": "string",
    "description": "string",
    "books": [
      {
        "id": "string",
        "isbn": "string",
        "name": "string",
        "author": "string",
        "genre": "string",
        "shelf": "string",
        "taken_by": "string",
        "taken_untill": "string",
        "reserved_by": "string"
      }
    ]
  }
]
```
# API Endpoint
- Tasks:
    - 0. add a new shelf in the library
- Method: `POST`
- Path: `/api/v1/shelves`
- Can access: `Librarian`

### Request Body

| Name | Type | Description |
| ---- | ---- | ----------- |
| `number` | `number` | The number of the shelf. |

### Responses

| Status Code | Description |
| ----------- | ----------- |
| `201` | Successfully created a new shelf. |
| `400` | The request body is invalid. |
| `500` | Something went wrong on the server. |

### Response Body on Success

This endpoint does not return a response body on success.

