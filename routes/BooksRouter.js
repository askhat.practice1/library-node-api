const express = require('express')
const router = express.Router()

const {
    searchBooks,
    putBook,
    getBook,
    moveBook,
    searchTakenBooks,
    takeBook,
    returnBook,
    reserveBook,
    cancelBookReservation,
    myReservedList,
} = require('../controllers/BooksController')

router.route('/').get(searchBooks).post(putBook)
router.route('/:bookId').get(getBook)
router.route('/:bookId/shelf/:shelfId').put(moveBook)
router.route('/take_book/:bookId').put(takeBook).delete(returnBook)
router.route('/taken_books/list').get(searchTakenBooks)
router.route('/reserve_book/:bookId').put(reserveBook).delete(cancelBookReservation)
router.route('/my_reserved_books/list').get(myReservedList)
module.exports = router
