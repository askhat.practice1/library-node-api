const express = require('express')
const router = express.Router()

const {
    get_all_shelves,
    add_new_shelf,
    get_all_books_on_the_shelf,
} = require('../controllers/ShelvesController')

router.route('/').get(get_all_shelves).post(add_new_shelf)
router.route('/:shelfId').get(get_all_books_on_the_shelf)

module.exports = router
