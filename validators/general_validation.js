const Joi = require('joi');

const isEven = (value, helpers) => {
    if (value % 2 !== 0) {
        return helpers.message('readerId must be even');
    }
    return value;
};

const isOdd = (value, helpers) => {
    if (value % 2 !== 1) {
        return helpers.message('librarinId must be odd');
    }
    return value;
};

const schema = Joi.object({
    userId: Joi.number().integer().min(1).required(),
    readerId: Joi.number().min(1).custom(isEven),
    librarianId: Joi.number().min(1).custom(isOdd),
    bookId: Joi.string().uuid(),
    shelfId: Joi.number().integer().min(1),
    period: Joi.number().integer().min(1),

    ISBN: Joi.string(),
    name: Joi.string(),
    author: Joi.string(),
    genre: Joi.string(),
    book: Joi.object({
        ISBN: Joi.string().required(),
        name: Joi.string().required(),
        author: Joi.string().required(),
        genre: Joi.string().required(),
        shelfId: Joi.number().integer().min(1).required(),
    })
});

module.exports = schema;

module.exports = schema;
