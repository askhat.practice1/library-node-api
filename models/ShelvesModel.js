const JSONDatabaseModel = require('./JSONDatabaseModel');
const { createCustomError } = require('../errors/custom-error');
const scheme = require('../validators/general_validation');

class ShelvesModel extends JSONDatabaseModel {
    constructor(file) {
        super(file);
    }

    async add_shelf(shelfId) {
        // action block
        const db = await this.read_db();
        db.shelves = [...new Set([...db.shelves, shelfId])];
        await this.write_db(db);
        return db.shelves;
    }

    async get_shelves() {
        const db = await this.read_db();
        return db.shelves;
    }

    async get_shelf(shelfId) {
        const db = await this.read_db();
        if (!db.shelves.includes(shelfId)) {
            throw createCustomError("Shelf not found", 404);
        }
        const filteredBooks = {};
        for (const id in db.books) {
            if (db.books[id].shelfId === shelfId) {
                filteredBooks[id] = db.books[id];
            }
        }
        return filteredBooks;
    }
}

module.exports = ShelvesModel;
