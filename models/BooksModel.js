const { v4: uuidv4 } = require('uuid');
const JSONDatabaseModel = require('./JSONDatabaseModel');
const { createCustomError } = require('../errors/custom-error');

class BooksModel extends JSONDatabaseModel {
    constructor(file) {
        super(file);
    }

    async get_book(id) {
        const db = await this.read_db();
        if (db.books[id]) {
            return db.books[id];
        } else {
            throw createCustomError("Book not found", 404);
        }
    }

    async put_book(new_book) {
        // check if shelfId exists
        const db = await this.read_db();
        if (!db.shelves.includes(new_book.shelfId)) {
            throw createCustomError("shelfId not found", 404);
        }
        // populate with extra details 
        new_book.id = uuidv4();
        new_book.taken_by = null;
        new_book.taken_until = null;
        new_book.reserved_by = null;
        // everything seems to be ok, add the book
        db.books[new_book.id] = new_book;
        await this.write_db(db);
        return new_book;
    }

    async move_book(bookId, shelfId) {
        // check if shelf exists
        const db = await this.read_db();
        if (!db.shelves.includes(shelfId)) {
            throw createCustomError("Shelf not found", 404);
        }
        // check if book exists
        if (!db.books[bookId]) {
            throw createCustomError("Book not found", 404);
        }
        // action block 
        db.books[bookId].shelfId = shelfId;
        await this.write_db(db);
        return db.books[bookId];
    }

    async get_all_books() {
        const db = await this.read_db();
        return db.books;
    }

    async take_book(bookId, userId, period) {
        const db = await this.read_db();
        // check if book exists
        if (!db.books[bookId]) {
            throw createCustomError("Book not found", 404);
        }
        // check if book is already taken
        if (db.books[bookId].taken_by) {
            throw createCustomError("Book is already taken", 400);
        }
        // check if it is reserved by someone else
        if (db.books[bookId].reserved_by && db.books[bookId].reserved_by != userId) {
            throw createCustomError("Book is reserved by someone else", 400);
        }
        // check if period is valid
        if (isNaN(period)) {
            throw createCustomError("period is invalid", 400);
        }
        // action block 
        var today = new Date();
        var taken_until = new Date(today.getTime() + period * 24 * 60 * 60 * 1000);
        taken_until.setHours(0, 0, 0, 0); // Set time to midnight
        db.books[bookId].taken_by = userId;
        db.books[bookId].taken_until = taken_until;
        await this.write_db(db);
        return db.books[bookId];
    }

    async return_book(bookId) {
        const db = await this.read_db();
        // check if book already exists
        if (!db.books[bookId]) {
            throw createCustomError("Book not found", 404);
        }
        // check if book is already taken
        if (!db.books[bookId].taken_by) {
            throw createCustomError("Book is not taken", 400);
        }
        db.books[bookId].taken_by = null;
        db.books[bookId].taken_until = null;
        await this.write_db(db);
        return db.books[bookId];
    }

    async reserve_book(bookId, userId) {
        const db = await this.read_db();
        // check if book exists
        if (!db.books[bookId]) {
            throw createCustomError("Book not found", 404);
        }
        // check if book is already taken
        if (!db.books[bookId].taken_by) {
            throw createCustomError("Book is not taken", 400);
        }
        // check if it is reserved by someone else
        if (db.books[bookId].reserved_by) {
            throw createCustomError("Book is already reserved", 400);
        }
        // action block 
        db.books[bookId].reserved_by = userId;
        await this.write_db(db);
        return db.books[bookId];
    }

    async unreserve_book(bookId, userId) {
        const db = await this.read_db();
        // check if book already exists
        if (!db.books[bookId]) {
            throw createCustomError("Book not found", 404);
        }
        // check if book is already taken
        if (!db.books[bookId].taken_by) {
            throw createCustomError("Book is not reserved", 400);
        }
        // check if it is reserved by someone else
        if (!db.books[bookId].reserved_by) {
            throw createCustomError("Book is not reserved", 400);
        }
        // check if it is reserved by someone else
        if (db.books[bookId].reserved_by && db.books[bookId].reserved_by != userId) {
            throw createCustomError("Book is reserved by someone else", 400);
        }
        db.books[bookId].reserved_by = null;
        await this.write_db(db);
        return db.books[bookId];
    }

    async search_books(query) {
        const db = await this.read_db();
        const books = db.books;
        let result = [];
        for (let book of books) {
            if (book.name.toLowerCase().includes(query.toLowerCase())) {
                result.push(book);
            }
        }
        return result;
    }

}


module.exports = BooksModel;
