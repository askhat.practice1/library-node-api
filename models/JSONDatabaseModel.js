const fs = require('fs');

const emptyDB = {
    "shelves": [],
    "books": {},
};

class JSONDatabase {
    constructor(filename) {
        this.filename = filename;
    }

    read_db() {
        let rawData = '';
        try {
            rawData = fs.readFileSync(this.filename);
        } catch (err) {
            if (err.code === 'ENOENT') {
                // If file doesn't exist, return empty JSON object
                return emptyDB;
            }
            // If there's some other error, throw it
            throw err;
        }
        return JSON.parse(rawData);
    }

    write_db(data) {
        fs.writeFileSync(this.filename, JSON.stringify(data, null, 2));
    }
}

module.exports = JSONDatabase;
